require 'pathname'
require 'colorize'

module Esri
  class ToJson
    attr_reader :path_to_file, :output_folder

    def self.convert(path_to_file, output_folder = nil)
      converter = new(path_to_file, output_folder)
      converter.convert!
    end

    def initialize(path_to_file, output_folder = nil)
      @path_to_file = Pathname.new(path_to_file)
      validate_file
      @output_folder = Pathname.new(output_folder || path_to_file)
      validate_output_folder
    end

    def convert!
      raise 'Already exist' if output_file_path.exist?

      p '######'
      p "start converting #{path_to_file}"

      `ogr2ogr -t_srs EPSG:4326 -dim 2 -lco ENCODING=UTF-8 -f 'GeoJSON' \
       -skipfailures #{output_file_path} #{path_to_file}`

      if output_file_path.exist?
        puts 'converting is successful'.green
      else
        puts 'converting is fail.'.red
      end
    ensure
      output_file_path.exist?
      p '######'
    end

    private

    def output_file_path
      @output_file_path ||= output_folder + output_file_name
    end

    def output_file_name
      @output_file_name ||=
        Pathname.new("#{path_to_file.basename('.shp')}.json")
    end

    def validate_file
      unless path_to_file.exist?
        raise ArgumentError, "Can\'t find file by path: \"#{path_to_file}\"."
      end

      return if path_to_file.extname == '.shp'
      raise ArgumentError, 'File need to be .shp extension.'
    end

    def validate_output_folder
      return if output_folder.exist?
      raise ArgumentError, "No such folder: \"#{output_folder}\"."
    end
  end
end
